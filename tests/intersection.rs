use float_cmp;

use intersection::{intersect_at, Geometry, Line, Point};

#[test]
fn none_equals_itself() {
    float_cmp::assert_approx_eq!(Geometry, Geometry::None, Geometry::None);
}

#[test]
fn point_equals_itself() {
    let point = Geometry::Point(Point(14.7, -1.2));
    float_cmp::assert_approx_eq!(Geometry, point, point);
}

#[test]
fn line_equals_itself() {
    let line = Geometry::Line(Line(Point(-68.2, 13.4), Point(-37.1, 18.6)));
    float_cmp::assert_approx_eq!(Geometry, line, line);
}

#[test]
fn line_equals_its_reverse() {
    let start = Point(-68.2, 13.4);
    let end = Point(-37.1, 18.6);
    let fwd = Geometry::Line(Line(start, end));
    let rev = Geometry::Line(Line(end, start));
    float_cmp::assert_approx_eq!(Geometry, fwd, rev);
}

#[test]
fn two_distinct_points() {
    let a_pt = Point(0.0, 0.0);
    let b_pt = Point(1.0, 2.0);
    let a = Line(a_pt, a_pt);
    let b = Line(b_pt, b_pt);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
fn two_same_points() {
    let a = Point(0.0, 0.0);
    let aa = Line(a, a);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(aa, aa), Geometry::Point(a));
}

#[test]
fn two_parallel_lines() {
    let a = Line(Point(1.0, 1.0), Point(-2.0, -3.0));
    let b = Line(Point(1.0, -2.0), Point(-2.0, -6.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
fn two_parallel_lines_reverse_direction() {
    let a = Line(Point(1.0, 1.0), Point(-2.0, -3.0));
    let b = Line(Point(-2.0, -6.0), Point(1.0, -2.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
fn non_intersecting_non_parallel_lines() {
    let a = Line(Point(6.0, -5.0), Point(4.0, -10.0));
    let b = Line(Point(-2.0, -6.0), Point(-8.0, -5.5));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
fn a_points_at_b_without_intersection() {
    let a = Line(Point(0.0, 8.0), Point(8.0, 0.0));
    let b = Line(Point(-6.0, -6.0), Point(-1.0, -1.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
fn a_points_away_from_b_without_intersection() {
    let a = Line(Point(0.0, 8.0), Point(8.0, 0.0));
    let b = Line(Point(-1.0, -1.0), Point(-6.0, -6.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
fn b_points_at_a_without_intersection() {
    let a = Line(Point(-6.0, -6.0), Point(-1.0, -1.0));
    let b = Line(Point(0.0, 8.0), Point(8.0, 0.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
fn b_points_away_from_a_without_intersection() {
    let a = Line(Point(-1.0, -1.0), Point(-6.0, -6.0));
    let b = Line(Point(0.0, 8.0), Point(8.0, 0.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
fn normal_intersection_on_integer() {
    let a = Line(Point(-3.0, -2.0), Point(6.0, 4.0));
    let b = Line(Point(-1.0, -6.0), Point(5.0, 6.0));
    float_cmp::assert_approx_eq!(
        Geometry,
        intersect_at(a, b),
        Geometry::Point(Point(3.0, 2.0))
    );
}

#[test]
#[ignore]
fn normal_intersection_on_decimal() {
    let a = Line(
        Point(-2.6666666667, -1.6666666667),
        Point(6.3333333, 4.3333333),
    );
    let b = Line(
        Point(-0.6666666667, -5.6666666667),
        Point(5.3333333, 6.3333333),
    );
    float_cmp::assert_approx_eq!(
        Geometry,
        intersect_at(a, b),
        Geometry::Point(Point(3.33333333, 2.33333333))
    );
}

#[test]
#[ignore]
fn very_small_intersection() {
    let a = Line(Point(-3e-21, -2e-21), Point(6e-21, 4e-21));
    let b = Line(Point(-1e-21, -6e-21), Point(5e-21, 6e-21));
    float_cmp::assert_approx_eq!(
        Geometry,
        intersect_at(a, b),
        Geometry::Point(Point(3e-21, 2e-21))
    );
}

#[test]
#[ignore]
fn very_big_intersection() {
    let a = Line(Point(-3e37, -2e37), Point(6e37, 4e37));
    let b = Line(Point(-1e38, -6e37), Point(5e37, 6e37));
    float_cmp::assert_approx_eq!(
        Geometry,
        intersect_at(a, b),
        Geometry::Point(Point(3e37, 2e37))
    );
}

#[test]
fn point_a_on_line_b() {
    let a_pt = Point(0.0, 3.0);
    let a = Line(a_pt, a_pt);
    let b = Line(Point(0.0, -315.0), Point(0.0, 2021.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(a_pt));
}

#[test]
fn point_b_on_line_a() {
    let a = Line(Point(0.0, -315.0), Point(0.0, 2021.0));
    let b_pt = Point(0.0, 3.0);
    let b = Line(b_pt, b_pt);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(b_pt));
}

#[test]
fn line_a_starts_on_line_b() {
    let a_start = Point(-132.0, 872.0);
    let a = Line(a_start, Point(142.0, -857.0));
    let b = Line(Point(-3000.0, 872.0), Point(3000.0, 872.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(a_start));
}

#[test]
fn line_a_ends_on_line_b() {
    let a_end = Point(-132.0, 872.0);
    let a = Line(Point(142.0, -857.0), a_end);
    let b = Line(Point(-3000.0, 872.0), Point(3000.0, 872.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(a_end));
}

#[test]
fn line_b_starts_on_line_a() {
    let a = Line(Point(-3000.0, 872.0), Point(3000.0, 872.0));
    let b_start = Point(-132.0, 872.0);
    let b = Line(b_start, Point(142.0, -857.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(b_start));
}

#[test]
fn line_b_ends_on_line_a() {
    let a = Line(Point(-3000.0, 872.0), Point(3000.0, 872.0));
    let b_end = Point(-132.0, 872.0);
    let b = Line(Point(142.0, -857.0), b_end);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(b_end));
}

#[test]
fn line_a_start_is_line_b_start_different_direction() {
    let shared_start = Point(0.0, 0.0);
    let a = Line(shared_start, Point(-1358.72, -14297.0001));
    let b = Line(shared_start, Point(4567.89, -232425.2));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_start));
}

#[test]
fn line_a_start_is_line_b_end_different_direction() {
    let shared_point = Point(0.0, 0.0);
    let a = Line(shared_point, Point(-1358.72, -14297.0001));
    let b = Line(Point(4567.89, -232425.2), shared_point);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_point));
}

#[test]
fn line_a_end_is_line_b_start_different_direction() {
    let shared_point = Point(0.0, 0.0);
    let a = Line(Point(-1358.72, -14297.0001), shared_point);
    let b = Line(shared_point, Point(4567.89, -232425.2));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_point));
}

#[test]
#[ignore]
fn line_a_end_is_line_b_end_different_direction() {
    let shared_end = Point(0.0, 0.0);
    let a = Line(Point(0.0, -4.0), shared_end);
    let b = Line(Point(4567.89, -232425.2), shared_end);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_end));
}

#[test]
#[ignore]
fn line_a_start_is_line_b_start_reverse_direction() {
    let shared_start = Point(5.0, -3.0);
    let a = Line(shared_start, Point(0.0, -4.0));
    let b = Line(shared_start, Point(20.0, 0.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_start));
}

#[test]
#[ignore]
fn line_a_start_is_line_b_end_reverse_direction() {
    let shared_point = Point(5.0, -3.0);
    let a = Line(shared_point, Point(0.0, -4.0));
    let b = Line(Point(20.0, 0.0), shared_point);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_point));
}

#[test]
#[ignore]
fn line_a_end_is_line_b_start_reverse_direction() {
    let shared_point = Point(5.0, -3.0);
    let a = Line(Point(0.0, -4.0), shared_point);
    let b = Line(shared_point, Point(20.0, 0.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_point));
}

#[test]
#[ignore]
fn line_a_end_is_line_b_end_reverse_direction() {
    let shared_end = Point(5.0, -3.0);
    let a = Line(Point(0.0, -4.0), shared_end);
    let b = Line(Point(20.0, 0.0), shared_end);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_end));
}

#[test]
#[ignore]
fn the_same_line() {
    let line = Line(Point(10.0, 22.0), Point(8.0, -15.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(line, line), Geometry::Line(line));
}

#[test]
#[ignore]
fn the_same_line_with_endpoints_reversed() {
    let start = Point(10.0, 22.0);
    let end = Point(8.0, -15.0);
    let a = Line(start, end);
    let b = Line(end, start);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Line(a));
}

#[test]
#[ignore]
fn same_infinite_line_overlapping_endpoints() {
    let a_end = Point(3.0, 3.0);
    let a = Line(Point(-10.0, -10.0), a_end);
    let b_start = Point(-4.0, -4.0);
    let b = Line(b_start, Point(20.0, 20.0));
    let predicted = Line(b_start, a_end);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Line(predicted));
}

#[test]
fn same_infinite_line_no_overlap() {
    let a = Line(Point(-50.0, 50.0), Point(3.0, -3.0));
    let b = Line(Point(100.0, -100.0), Point(5.0, -5.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
fn same_infinite_line_no_overlap_a_is_point() {
    let a_pt = Point(0.0, 200.0);
    let a = Line(a_pt, a_pt);
    let b = Line(Point(0.0, -255.0), Point(0.0, 75.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
fn same_infinite_line_no_overlap_b_is_point() {
    let a = Line(Point(0.0, -255.0), Point(0.0, 75.0));
    let b_pt = Point(0.0, 200.0);
    let b = Line(b_pt, b_pt);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
fn same_infinite_line_a_entirely_in_b() {
    let a = Line(Point(0.0, 8.0), Point(0.0, 10.0));
    let b = Line(Point(0.0, 6.0), Point(0.0, 12.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Line(a));
}

#[test]
fn same_infinite_line_b_entirely_in_a() {
    let a = Line(Point(0.0, 6.0), Point(0.0, 12.0));
    let b = Line(Point(0.0, 8.0), Point(0.0, 10.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Line(b));
}

#[test]
fn a_is_startpoint_of_line_b() {
    let shared_pt = Point(-12.5, 37.0);
    let a = Line(shared_pt, shared_pt);
    let b = Line(shared_pt, Point(13.0, -1200.0));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_pt));
}

#[test]
fn b_is_startpoint_of_line_a() {
    let shared_pt = Point(-12.5, 37.0);
    let a = Line(shared_pt, Point(13.0, -1200.0));
    let b = Line(shared_pt, shared_pt);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_pt));
}

#[test]
fn a_is_endpoint_of_line_b() {
    let shared_pt = Point(-12.5, 37.0);
    let a = Line(shared_pt, shared_pt);
    let b = Line(Point(13.0, -1200.0), shared_pt);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_pt));
}

#[test]
fn b_is_endpoint_of_line_a() {
    let shared_pt = Point(-12.5, 37.0);
    let a = Line(Point(13.0, -1200.0), shared_pt);
    let b = Line(shared_pt, shared_pt);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::Point(shared_pt));
}

#[test]
fn two_points_very_close_together() {
    let pt_a = Point(std::f32::consts::PI, std::f32::consts::E);
    let pt_b = Point(3.14159, 2.71828);
    let a = Line(pt_a, pt_a);
    let b = Line(pt_b, pt_b);
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
#[ignore]
fn two_parallel_lines_very_close_together() {
    let a = Line(Point(1.0, 3.0), Point(10.0, 12.0));
    let b = Line(Point(1.000001, 3.000001), Point(10.000001, 12.000001));
    float_cmp::assert_approx_eq!(Geometry, intersect_at(a, b), Geometry::None);
}

#[test]
#[ignore]
fn two_nearly_parallel_lines_very_close_together() {
    let a = Line(Point(-1e8, 1.0), Point(1e8, 1.0));
    let b = Line(Point(1e8, 0.0), Point(-1e8, 0.0));
    float_cmp::assert_approx_eq!(
        Geometry,
        intersect_at(a, b),
        Geometry::Point(Point(0.0, 0.5))
    );
}

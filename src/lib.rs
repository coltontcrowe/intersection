use float_cmp::{approx_eq, ApproxEq};
use num_traits::Zero;
use ordered_float;

use std::ops::{Add, Mul, Neg, Sub};

#[derive(Copy, Clone, Debug, Default, Hash, PartialEq, PartialOrd, Eq, Ord)]
pub struct Point<T>(pub T, pub T);

impl<T> Point<T>
where
    T: Mul<T, Output = T> + Sub<T, Output = T> + Add<T, Output = T> + Copy,
{
    fn wedge(self, other: Point<T>) -> T {
        self.0 * other.1 - self.1 * other.0
    }

    fn dot(self, other: Point<T>) -> T {
        self.0 * other.0 + self.1 * other.1
    }
}

impl<T: Zero + ApproxEq + Copy> Zero for Point<T> {
    fn zero() -> Self {
        Point(T::zero(), T::zero())
    }

    fn is_zero(&self) -> bool {
        approx_eq!(T, self.0, T::zero()) && approx_eq!(T, self.1, T::zero())
    }
}

impl<M: Copy + Default, F: Copy + ApproxEq<Margin = M>> ApproxEq for Point<F> {
    type Margin = M;

    fn approx_eq<T: Into<Self::Margin>>(self, other: Self, margin: T) -> bool {
        let margin = margin.into();
        self.0.approx_eq(other.0, margin) && self.1.approx_eq(other.1, margin)
    }
}

impl<'a, M: Copy + Default, F: Copy + ApproxEq<Margin = M>> ApproxEq for &'a Point<F> {
    type Margin = M;

    fn approx_eq<T: Into<Self::Margin>>(self, other: Self, margin: T) -> bool {
        let margin = margin.into();
        self.0.approx_eq(other.0, margin) && self.1.approx_eq(other.1, margin)
    }
}

impl<T: Add<Output = T>> Add for Point<T> {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0, self.1 + other.1)
    }
}

impl<T: Neg<Output = T>> Neg for Point<T> {
    type Output = Self;

    fn neg(self) -> Self {
        Self(-self.0, -self.1)
    }
}

impl<T: Sub<Output = T>> Sub for Point<T> {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self(self.0 - other.0, self.1 - other.1)
    }
}

impl<T, U, V> Mul<U> for Point<T>
where
    T: Mul<U, Output = V> + Copy,
    U: Copy,
    V: Copy,
{
    type Output = Point<V>;

    fn mul(self, scalar: U) -> Self::Output {
        Point(self.0 * scalar, self.1 * scalar)
    }
}

#[derive(Copy, Clone, Debug, Default, Hash, PartialEq, PartialOrd, Eq, Ord)]
pub struct Line<T>(pub Point<T>, pub Point<T>);

impl Line<f32> {
    fn line_intersect(self, other: Self) -> Geometry {
        let a_vec = self.1 - self.0;
        let b_vec = other.1 - other.0;
        let edge = other.0 - self.0;

        let area = a_vec.wedge(b_vec);
        let t_area = edge.wedge(b_vec);
        let u_area = edge.wedge(a_vec);

        match (area, t_area, u_area) {
            (a, ta, _) if a.is_zero() && ta.is_zero() => self.shared_intersect(other), // same line
            (a, _, ua) if a.is_zero() && ua.is_zero() => self.shared_intersect(other), // same line
            (a, _, _) if a.is_zero() => Geometry::None,                                // parallel
            (_, _, _) => {
                let t = t_area / area;
                let u = u_area / area;
                if t >= 0.0 && t <= 1.0 && u >= 0.0 && u <= 1.0 {
                    let result = self.0 * (1.0 - t) + self.1 * t;
                    Geometry::Point(result)
                } else {
                    Geometry::None
                }
            }
        }
    }

    fn shared_intersect(self, other: Self) -> Geometry {
        let vec = self.1 - self.0;
        let mut endpts = [self.0, self.1, other.0, other.1];
        endpts.sort_by_key(|&pt| {
            let rel = pt - self.0;
            ordered_float::OrderedFloat(rel.dot(vec))
        });
        match endpts {
            [a, b, _, _] if approx_eq!(Line<f32>, self, Line(a, b)) => Geometry::None,
            [a, b, _, _] if approx_eq!(Line<f32>, other, Line(a, b)) => Geometry::None,
            [_, a, b, _] => Geometry::Line(Line(a, b)),
        }
    }
}

trait Intersection<T> {
    fn intersection(self, other: T) -> Geometry;
}

impl Intersection<Line<f32>> for Line<f32> {
    fn intersection(self, other: Self) -> Geometry {
        let a_vec = self.1 - self.0;
        let b_vec = other.1 - other.0;
        let edge_c = other.0 - self.0;

        match (a_vec, b_vec, edge_c) {
            (a, b, c) if a.is_zero() && b.is_zero() && c.is_zero() => Geometry::Point(self.0),
            (a, b, _) if a.is_zero() && b.is_zero() => Geometry::None,
            (a, _, c) if a.is_zero() && c.is_zero() => Geometry::Point(self.0),
            (_, b, c) if b.is_zero() && c.is_zero() => Geometry::Point(other.0),
            (a, _, _) if a.is_zero() => other.intersection(self.0),
            (_, b, _) if b.is_zero() => self.intersection(other.0),
            (_, _, _) => self.line_intersect(other),
        }
    }
}

impl Intersection<Point<f32>> for Line<f32> {
    fn intersection(self, other: Point<f32>) -> Geometry {
        let vec = self.1 - self.0;
        let rel = other - self.0;

        if approx_eq!(f32, vec.wedge(rel), f32::zero()) {
            let t = vec.dot(rel);
            let len2 = vec.dot(vec);
            if t >= 0.0 && t <= len2 {
                Geometry::Point(other)
            } else {
                Geometry::None
            }
        } else {
            Geometry::None
        }
    }
}

impl<M: Copy + Default, F: Copy + ApproxEq<Margin = M>> ApproxEq for Line<F> {
    type Margin = M;

    fn approx_eq<T: Into<Self::Margin>>(self, other: Self, margin: T) -> bool {
        let margin = margin.into();
        (self.0.approx_eq(other.0, margin) && self.1.approx_eq(other.1, margin))
            || (self.0.approx_eq(other.1, margin) && self.1.approx_eq(other.0, margin))
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Geometry {
    None,
    Point(Point<f32>),
    Line(Line<f32>),
}

impl Default for Geometry {
    fn default() -> Self {
        Self::None
    }
}

impl ApproxEq for Geometry {
    type Margin = (f32, i32);

    fn approx_eq<T: Into<Self::Margin>>(self, other: Self, margin: T) -> bool {
        let margin = margin.into();
        match (self, other) {
            (Self::None, Self::None) => true,
            (Self::Point(pt0), Self::Point(pt1)) => pt0.approx_eq(pt1, margin),
            (Self::Line(ln0), Self::Line(ln1)) => ln0.approx_eq(ln1, margin),
            _ => false,
        }
    }
}

pub fn intersect_at(a: Line<f32>, b: Line<f32>) -> Geometry {
    a.intersection(b)
}
